console.log("Hello World")

//Statements, Syntax and Comments


//console.log is used to print out 

//Variables 
//used to contain data

//Declare Variables:
// Syntax in declaring variables; let/const variableName; var

//let/const/var is a keyword usually used in declaring a variable.

//let myVariable
const myVariable ="Hello again";
console.log(myVariable);

//Declaring and initializing variables
//Initializing variables -the instance when a variable is given it's initial/starting value
//Syntax: let/const variableName = value;
let productName = "desktop computer";
let productPrice = 18999;
console.log(productPrice)

const interest = 3.539;

//Reassigning variable values

productName = 'Laptop';
console.log(productName);

//let variable can be reassigned with another value;

let friend = 'Kate';
let anotherFriend = 'John';

const pi = 3.14;





let country = 'Philippines';
let province = 'Metro Manila';
console.log(typeof country);
console.log(typeof province);
//Template literals (ES6) is the updated version of concatenation
//bacticks 'dvdv'
// expression ${}
console.log('I live in the ${provice}, ${country}');

//Escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text


let firstname = 'Kyokonesa';
let lastname = 'Delfin';
console.log(firstname, lastname);


let sentence = (`My name is ${firstname}, ${lastname}. I am a Zuitt bootcamper.`);
console.log(sentence);

const faveFood = ['chicken', 'steak', 'salad','fruits'];
console.log(faveFood);


//NUMBER

let numb1 = 5;
let numb2 = 6;
let numb3 =5.5;
let numb4 = .5;



//NULL
// it is used to intentionally express the absence of a value in a variable declaration/initialization

let girlfriend = null;
console.log(girlfriend)

//using null compared to a 0 value and an empty string is much better for readability purposes.



//UNDEFINED
//represents the state of a variable that has been declared but without an assigned value.
let sampleVariable;
console.log(sampleVariable); //undefined

// one clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.
//null means that a variable was created and was assigned a value that does not hold any value/amount.


//FUNCTION
//Function in JS, it is a line/lines/block of codes that tell our app to perform a cretain task when called/invoked.

//FUNCTION DECLARATION
	//defines a function with the function name and specified parameters.

/*
Syntax:

	function functionName() {
		code block/statements.  the block of code will be executed ince the function has been run/called/invoked.
	}

	function keyword => defined a js function
	function name => camelCase to name our function
	function block/statement => the statements which comprise the body of the function. This is where the code to be executed.


*/

function printName() {
	console.log("My name is Kyokonesa");
}


//FUNCTION EXPRESSION
	//A function expression can be stored in a variable

	let variableFunction = function() {
		console.log("Hello World")
	}

	let funcExpression = function func_name() {
		console.log("Hello!")
	}

//FUNCTION INVOCATION
	//the code inside a function is executed when the function is called/invoked.

//Lets's invoke/call the function that we declared

printName();

function declaredFunction() {
	console.log("Hello World")
}

declaredFunction();

//How about the function expression?
//They are always invoked/called using the variable name.
variableFunction();

//Function Scoping

//JS Scopr
//Global and Local Scope

let faveCharacter = "Nezuko-chan";

function myFunction() {
	let nickName = "Jane";
	console.log(nickName);
	console.log(faveCharacter);
}


myFunction();



function showSum() {
	console.log(6 + 6)
}

showSum();


//PARAMETERS AND ARGUMENTS
//"name" is called a parameter
//parameter => acts as a named variable/container that exists ONLY inside of the function. Usually parameter is the placeholder of an actual value
let name = "Jane Smith";


function printName(pikachu) {
		console.log(`My name is ${pikachu}`);
	};


//argument is the actual value/data that we passed to our function
//We can also use a variable as an argument
printName(name);

//Multiple Parameters and Argumments


function displayFullName(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`)
}



displayFullName("Judy", "Medalla", "jane", 67);
//Providing less arguments than the expected parameters will automatically assign an undefined value to the paramter.
//Providing more arguments will not affect our function.


//return keyword
	//The "return" statement allows the ouptut of a function to be passed to the block of code that invoked the function
	//any line/block of code that comes after the return statement is ignored because it ends the function execution

	//return keyword is used so that a function may return a value.
	//after returning the value, the next statement will stop its process

	function createFullName(firstName, middleName, lastName) {
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned")
	}

	//result of this function(return) can be saved into a variable

	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1);

	//The result of a function without a return keyword will not save the result in a variable;

	let fullName2 = displayFullName("William", "Bradley", "Pitt");
	console.log(fullName2);


	let fullName3 = createFullName("Jeffrey", "John", "Smith");
	console.log(fullName3);


/*
Mini Activity
	Create a function can receive two numbers as arguments.
		-display the quotient of the two numbers in the console.
		-return the value of the division

	Create a variable called quotient.
		-This variable should be able to receive the result of division function

	Log the quotient variable's value in the console with the following message:

	"The result of the division is: <value of Quotient>"
		-use concatenation/template literals

	Take a screenshot of your console and send it in the hangouts.

*/

//Stretch goals
//get the number with the use of prompt

function twoNumbers(n1, n2) {
	console.log( n1 / n2)
}

twoNumbers(176, 4)


let num1 = prompt("Enter first number:");

let num2 = prompt("Enter second number:");

let quotient = num1 / num2 

console.log(`The result of the division is: ${quotient}`)




function printUserInfo(first_name, last_name, age_, housenumber, street, city, state) {

	console.log(`First Name: ${first_name}`)
	console.log(`Last Name: ${last_name}`)
	console.log(`Age: ${age_}`)
	const hobbies = ['reading', 'writing', 'watching'];
	console.log(`Hobbies:` + hobbies)
	
	console.log(`house number: ${housenumber} street: ${street} city: ${city} state: ${state}`);

	console.log(`${first_name} ${last_name} is ${age_} years of age`);
	console.log(hobbies)

}
printUserInfo("Kyokonesa", "Delfin", 30, 32, "Elmo", "Los Angeles", "California")


let isMarried = false;
console.log(`The value of isMarried is ${isMarried}`)


function returnFunction(godzilla){

return `${godzilla}`
}




function addnumbers(number1, number2) {
	console.log(number1 + number2)
}

addnumbers(45, 65)


function subnumbers(no1, no2) {
	console.log(no1 + no2)
}

subnumbers(174, 65)


function multnumbers(nmb1, nmb2) {
	console.log(nmb1 + nmb2)
}

multnumbers(45, 65)


















